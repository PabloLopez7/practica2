
package pratica2_pablo_lopez;

import org.github.jamm.MemoryMeter;

public class Datos_Primitivos {
     public static void main(String[] args){
        
         //Datos Primitivos string, int, double, char, float, byte, long, boolean
        String Hola= "45";
        int A;    
        double B; 
        char C;   
        float D;
        long E; 
        boolean F; 
        boolean G;
        byte H; 
        
        A=2;
        B=3.0;
        C='B';
        D=65.35f;
        E=12345678910L;
        F= true;
        G= false;
        H=1;


        //Imprimiendo los valores
        System.out.println("Valor en formato Strig :  Hola" );
        System.out.println("Valor en formato int: " + A);
        System.out.println("Valor en formato double: " + B);
        System.out.println("Valor en formato char: " + C);
        System.out.println("Valor en formato float: " + D);
        System.out.println("Valor en formato long: " + E);
        System.out.println("Valor en formato boolean " + F);
        System.out.println("Valor en formato boolean: " + G);
        System.out.println("Valor en formato byte: " + H);
        
        
        
        //Espacio de memoria del string
        System.out.println("Espacio de memoria : ");
        
        MemoryMeter a= MemoryMeter.builder().build();
        System.out.println(a.measureDeep(Hola));
        System.out.println(MemoryUtil.deepSizeOf(Hola));
        
        //Espacio de memoria del int
        MemoryMeter b= MemoryMeter.builder().build();
        System.out.println(b.measureDeep(A));
        System.out.println(MemoryUtil.deepSizeOf(A));
        
        //Espacio de memoria del double
        MemoryMeter c= MemoryMeter.builder().build();
        System.out.println(c.measureDeep(B));
        System.out.println(MemoryUtil.deepSizeOf(B));
       
        //Espacio de memoria del char
        MemoryMeter e= MemoryMeter.builder().build();
        System.out.println(e.measureDeep(C));
        System.out.println(MemoryUtil.deepSizeOf(C));
        
        //Espacio de memoria del float
        MemoryMeter f= MemoryMeter.builder().build();
        System.out.println(f.measureDeep(D));
        System.out.println(MemoryUtil.deepSizeOf(D));
      
        //Espacio de memoria del long
        MemoryMeter g= MemoryMeter.builder().build();
        System.out.println(g.measureDeep(E));
        System.out.println(MemoryUtil.deepSizeOf(E));
        
        //Espacio de memoria del Boolean
        MemoryMeter h= MemoryMeter.builder().build();
        System.out.println(h.measureDeep(F));
        System.out.println(MemoryUtil.deepSizeOf(F));
        
        //Espacio de memoria del boolena 
        MemoryMeter j= MemoryMeter.builder().build();
        System.out.println(j.measureDeep(G));
        System.out.println(MemoryUtil.deepSizeOf(G));
        
        //Formato de memoria de Byte
        MemoryMeter k= MemoryMeter.builder().build();
        System.out.println(k.measureDeep(H));
        System.out.println(MemoryUtil.deepSizeOf(H));
    }    
}
